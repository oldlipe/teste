package br.calc;

public class Maratona implements Operacao {

	
	private int posfixo;
	private int sufixo;
	private String versao;
	private int primo;
	
	public Maratona() {}
	
	public void setPosfixo(int posfixo) {
		this.posfixo = posfixo;
	}
	public void setSufixo(int sufixo) {
		this.sufixo = sufixo;
	}
	
	public void setVersao(String versao) {
		this.versao = versao;
	}
	
	public void setPrimo(int primo) {
		this.primo = primo;
	}
	
	@Override
	public int getPrimoRapido() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int getPrimoLento() {
		return primo;
	}

	@Override
	public int getSoma() {
		return sufixo + posfixo;
	}

	@Override
	public int getSub() {
		return sufixo - posfixo;
	}

	@Override
	public int getDiv() {
		return sufixo / posfixo;
	}

	@Override
	public int getMult() {
		return sufixo * posfixo;
	}

	@Override
	public String getVersao() {
		return "Versao atual: " + versao;
	}

}
