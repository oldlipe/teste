package br.calc;

/*Calculdora
 * 
 * Classe que faz os calculos
 */

public class Calculadora {
	
	private Operacao operacao;
	
	public Calculadora() {}
	
	public String verificaPrimoLento() {
		long start = System.currentTimeMillis();
		return String.valueOf(primoLento(operacao.getPrimoLento())) + " Tempo estimado: " + 
				(System.currentTimeMillis() - start);
	}
	
	public String versao() {
		return "A versão da calculadora é " + operacao.getVersao();
	}
	
	public String verificaPrimoRapido() {
		long start = System.currentTimeMillis();
		return String.valueOf(primoRapido(operacao.getPrimoRapido())) + " Tempo estimado: " + 
				(start - System.currentTimeMillis());	
		}

	public void setOperacao(Operacao operacao) {
		this.operacao = operacao;
	}
	
	public boolean primoLento(int valor) {
		int divisores = 0;
		for (int i = 1; i <= valor; i++) {
			if (valor % i == 0) {
				divisores += 1;
			}
		}
		if (divisores == 2) {
			return true;
		} else {
			return false;
		}
	}
	
	public boolean primoRapido(int valor) {
		if (valor % 2 == 0) {
			return false;
		} else {
			int divisores = 0;
			Double raizValor = Math.sqrt(valor);
			for (int i = 1; i <= raizValor; i++) {
				if (raizValor % i == 0) {
					divisores += 1;
				}
			}
			if (divisores == 2) {
				return true;
			} else {
				return false;
			}
		}
	}

}
