package br.calc;
/* Operacao
 * 
 * Interface de operacoes
 */

public interface Operacao {
	
	public String getVersao();
	
	public int getSoma();
	
	public int getSub();
	
	public int getDiv();
	
	public int getMult();
	
	public int getPrimoRapido();
	
	public int getPrimoLento();
}
