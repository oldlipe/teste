package br.calc;

import org.springframework.context.support.ClassPathXmlApplicationContext;

/*
 * Grupo
 * 
 * @ Felipe Carvalho de Souza
 * 
 * @ Daniela Zaramello
 * 
 * @ Caroline Texeira
 * 
 * @ Gustavo Henrique
 * 
 * @ Italo Vassali
 */

public class Main {

	public static void main(String[] args) {
		ClassPathXmlApplicationContext context = 
				new ClassPathXmlApplicationContext(	"applicationContext.xml");

		/*
		 * Verifica versão da calculadora 
		 */
		Calculadora calc = (Calculadora) context.getBean("calc");
		System.out.println(calc.versao());
		
		/* 
		 * Opecaoes basicas
		 */
		Maratona maratonaSoma = (Maratona) context.getBean("operacaoSoma");
		System.out.println("Operacao de soma: " + maratonaSoma.getSoma());
		
		Maratona maratonaSub = (Maratona) context.getBean("operacaoSub");
		System.out.println("Operacao de subtracao: " + maratonaSub.getSub());
		
		/*
		 * Operacoes com os primos
		 */

		Calculadora calcPrimo = (Calculadora) context.getBean("calcPrimoLento");
		System.out.println(calcPrimo.verificaPrimoLento());
		
		Calculadora calcPrimoRa = (Calculadora) context.getBean("calcPrimoRapido");
		System.out.println(calcPrimoRa.verificaPrimoRapido());
		
		context.close();
	}

}
